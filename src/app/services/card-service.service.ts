import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Card } from '../components/card/card.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CardServiceService {
  constructor(private http: HttpClient) { }
  endpoint = 'http://localhost:3000';
  card = {} as Card;

  getAlgumaCoisa(): any {
    console.log(this.http.get(this.endpoint + '/cards'));
    const teste = this.http.get(this.endpoint + '/cards');
    return teste;
  }
  getCard(id): Observable<any> {
    console.log(this.http.get(this.endpoint + '/cards/' + id));

    return this.http.get(this.endpoint + '/cards/' + id);
  }
}

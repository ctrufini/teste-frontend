import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { CardComponent } from "./components/card/card.component";
import { CardDetailComponent } from "./components/card-detail/card-detail.component";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { AppRoutingModule } from "./app-routing.module";
import { HeaderComponent } from "./components/header/header.component";
import { FooterComponent } from "./components/footer/footer.component";
import { AuctionHouseComponent } from "./components/auction-house/auction-house.component";
import { DragScrollModule } from "ngx-drag-scroll";

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    CardDetailComponent,
    HeaderComponent,
    FooterComponent,
    AuctionHouseComponent
  ],
  imports: [BrowserModule, AppRoutingModule, DragScrollModule],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule {}

import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CardServiceService} from '../../services/card-service.service';
import {Card} from '../card/card.model';

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.css']
})
export class CardDetailComponent implements OnInit {

  constructor(public rest: CardServiceService, private route: ActivatedRoute, private router: Router) {
  }
  card: {} = {} as Card;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      console.log(params.get('id'));
      this.rest.getCard(params.get('id')).subscribe(c => {
        console.log(c);
        this.card = c;
      });
    });
  }

}

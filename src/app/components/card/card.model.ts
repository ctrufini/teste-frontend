export class Card {
  id: number;
  header: string;
  description: string;
}

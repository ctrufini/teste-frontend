import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {CardServiceService} from '../../services/card-service.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  constructor(
    public router: Router,
    public cardService: CardServiceService
  ) {
  }

  cards: any;
  loadCards() {
    this.cardService.getAlgumaCoisa().subscribe((data: {}) => {
      console.log(data);
      this.cards = data;
    });
  }

  gotoDetail(id): void {
    this.router.navigate(['detail', id]);
  }

  ngOnInit() {
    this.loadCards();
  }
}

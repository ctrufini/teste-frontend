import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { CardDetailComponent } from "./components/card-detail/card-detail.component";
import { HttpClientModule } from "@angular/common/http";
import { CardComponent } from "./components/card/card.component";
import { AuctionHouseComponent } from "./components/auction-house/auction-house.component";

const routes: Routes = [
  { path: "", redirectTo: "/cards", pathMatch: "full" },
  { path: "cards", component: CardComponent },
  { path: "auction/:id", component: AuctionHouseComponent },
  { path: "detail/:id", component: CardDetailComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes), HttpClientModule],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
